from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from main.models import Loan_Request
from django.http import HttpResponse
# Create your views here.

def loans(response):
	return render(response, "main/CCSEP_home_page.html")


def db_entry(request):
	if request.method == "POST":
		in_name = request.POST.get('Name')
		in_email = request.POST.get('Email')
		in_loan_type = request.POST.get('L_Type')
		in_age = request.POST.get('Age')
		in_loan_amount  = request.POST.get('L_Amount')

		Loan_Request.objects.create(name=in_name, email=in_email, loan_type=in_loan_type, age=in_age, loan_amount=in_loan_amount)

		return( render(request, "main/CCSEP_submitted.html"))

def login(request):
	if request.method == 'POST':

		...
	else:
		form = AuthenticationForm()
	return render(request, 'main/login.html', {'form':form})

def dashboard(request):
	return render(request, "main/CCSEP_Executive_Dashboard.html")

def live_feed(request):
	return_html = ""
	for item in Loan_Request.objects.all():
		name = item.name
		append_html="""<tr>
			<td><i class="fa fa-bell w3-text-red w3-large"></i></td>
			<td>{name} requested a loan</td>
			<td><i>Loans Department</i></td>
		</tr>""".format(name=name)

		return_html += append_html


	return HttpResponse(return_html)





