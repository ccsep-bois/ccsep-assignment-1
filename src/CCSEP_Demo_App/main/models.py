from django.db import models

# Create your models here.
class Loan_Request(models.Model):
	name = models.CharField(max_length = 50)
	email = models.CharField(max_length = 50)
	loan_type = models.CharField(max_length = 20)
	loan_amount = models.CharField(max_length = 20)
	age = models.CharField(max_length = 3)
